package com.exam.umar;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

public class StateAdapter extends RecyclerView.Adapter<StateAdapter.StateViewHolder> {
    private List<StateData> stateDataList;

    public void setData(List<StateData> data) {
        stateDataList = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public StateViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_state, parent, false);
        return new StateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StateViewHolder holder, int position) {
        StateData stateData = stateDataList.get(position);
        holder.stateName.setText(stateData.getState());
        holder.population.setText("Population: " + stateData.getPopulation());
        // Inside onBindViewHolder method, trigger the onItemClick when an item is clicked

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stateName = stateData.getState();
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(stateName);
                }
            }
        });
    }


    public interface OnItemClickListener {
        void onItemClick(String stateName);
    }
    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }


    @Override
    public int getItemCount() {
        return stateDataList != null ? stateDataList.size() : 0;
    }

    static class StateViewHolder extends RecyclerView.ViewHolder {
        TextView stateName;
        TextView population;

        public StateViewHolder(View itemView) {
            super(itemView);
            stateName = itemView.findViewById(R.id.stateName);
            population = itemView.findViewById(R.id.population);
        }
    }
}

