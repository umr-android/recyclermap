package com.exam.umar;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {
    @GET("https://datausa.io/api/data?drilldowns=State&measures=Population&year=latest")
    Call<StateResponse> getStateData();
}

