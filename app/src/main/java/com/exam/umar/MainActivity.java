package com.exam.umar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import okhttp3.OkHttpClient;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private StateAdapter stateAdapter;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        stateAdapter = new com.exam.umar.StateAdapter();
        stateAdapter.setOnItemClickListener(this::onItemClick); // Set the click listener

        progressBar = findViewById(R.id.progressBar);

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.setAdapter(stateAdapter);

        progressBar.setVisibility(View.VISIBLE);

        fetchData();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh the data
                fetchData();
            }
        });
    }

    public void onItemClick(String stateName) {
        Intent mapIntent = new Intent(this, MapsActivity.class);
        mapIntent.putExtra("stateName", stateName);
        startActivity(mapIntent);
    }

    private void fetchData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://datausa.io/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder().build())
                .build();

        ApiService apiService = retrofit.create(ApiService.class);

        Call<StateResponse> call = apiService.getStateData();
        call.enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                if (response.isSuccessful()) {
                    StateResponse stateResponse = response.body();
                    if (stateResponse != null) {
                        stateAdapter.setData(stateResponse.getData());
                    }
                }

                // Hide the progress bar and the refresh indicator
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                t.printStackTrace();

                // Hide the progress bar and the refresh indicator
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

}
