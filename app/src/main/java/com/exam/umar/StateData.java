package com.exam.umar;

import com.google.gson.annotations.SerializedName;

public class StateData {
    @SerializedName("ID State")
    private String idState;

    @SerializedName("State")
    private String state;

    @SerializedName("ID Year")
    private int idYear;

    @SerializedName("Year")
    private String year;

    @SerializedName("Population")
    private int population;

    @SerializedName("Slug State")
    private String slugState;

    // Getters and setters

    public String getState() {
        return state;
    }

    public int getPopulation() {
        return population;
    }
}

