package com.exam.umar;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StateResponse {
    @SerializedName("data")
    private List<StateData> data;

    public List<StateData> getData() {
        return data;
    }
}

