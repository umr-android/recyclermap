package com.exam.umar;

import androidx.fragment.app.FragmentActivity;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.exam.umar.databinding.ActivityMapsBinding;

import java.io.IOException;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ActivityMapsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Retrieve the state name from the intent
        String stateName = getIntent().getStringExtra("stateName");

        // Use stateName to display the location on the map
        displayLocation(stateName);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Retrieve the state name from the intent
        String stateName = getIntent().getStringExtra("stateName");

        displayLocation(stateName);
    }

    private void displayLocation(String stateName) {
        // Use stateName to set the marker and move the camera accordingly
        LatLng stateLatLng = getLatLngForState(stateName);

        if (stateLatLng != null && mMap != null) {
            mMap.addMarker(new MarkerOptions().position(stateLatLng).title(stateName));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(stateLatLng));
        }
    }

    private LatLng getLatLngForState(String stateName) {
        Geocoder geocoder = new Geocoder(this);
        List<Address> addresses;

        try {
            addresses = geocoder.getFromLocationName(stateName, 1);
            if (!((List<?>) addresses).isEmpty()) {
                Address address = addresses.get(0);
                double latitude = address.getLatitude();
                double longitude = address.getLongitude();
                return new LatLng(latitude, longitude);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
